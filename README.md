<!-- PROJECT SHIELDS -->
<!--
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!-- [![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url] -->




<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/azurka/resourcepack/azurkatexturepack">
    <img src="images/logo.png" alt="Logo" >
  </a>

  <h3 align="center">Azurka Project ResourcePack</h3>

  <p align="center">
    A ajouter

  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Azurka Project</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="https://gitlab.com/azurka/plugins">Plugins</a>
      <ul>
        <li><a href="https://gitlab.com/azurka/plugins/guilds">Guilds</a></li>
      </ul>
    </li>
    <li>
      <a href="https://gitlab.com/azurkaresourcepack">Resources</a>
      <ul>
        <li><a href="https://gitlab.com/azurka/resourcepack/azurkatexturepack">TexturePack</a></li>
      </ul>
    </li>
    <li>
      <a href="https://gitlab.com/azurka/web">Web</a>
    </li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
<!--
[![Product Name Screen Shot][product-screenshot]](https://example.com)


`azurka`, `resourcepack/azurkatexturepack`, `projetazurka`, `contact.azurka@gmail.com`, `Azurka Project`
-->
A compléter


<!-- LICENSE -->
## License

See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Azurka's team - [@projetazurka](https://twitter.com/projetazurka) - contact.azurka@gmail.com

Project Link: [https://gitlab.com/azurka/](https://github.com/azurka/)









<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links 
[contributors-shield]: https://img.shields.io/gitlab/contributors/azurka/resourcepack/azurkatexturepack.svg?style=for-the-badge
[contributors-url]: https://github.com/azurka/resourcepack/azurkatexturepack/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/azurka/resourcepack/azurkatexturepack.svg?style=for-the-badge
[forks-url]: https://github.com/azurka/resourcepack/azurkatexturepack/network/members
[stars-shield]: https://img.shields.io/github/stars/azurka/resourcepack/azurkatexturepack.svg?style=for-the-badge
[stars-url]: https://github.com/azurka/resourcepack/azurkatexturepack/stargazers
[issues-shield]: https://img.shields.io/github/issues/azurka/resourcepack/azurkatexturepack.svg?style=for-the-badge
[issues-url]: https://github.com/azurka/resourcepack/azurkatexturepack/issues
[license-shield]: https://img.shields.io/github/license/azurka/resourcepack/azurkatexturepack.svg?style=for-the-badge
[license-url]: https://github.com/azurka/resourcepack/azurkatexturepack/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555 -->

